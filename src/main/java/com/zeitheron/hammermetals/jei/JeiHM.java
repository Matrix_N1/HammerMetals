package com.zeitheron.hammermetals.jei;

import java.util.Objects;
import java.util.stream.Collectors;

import com.zeitheron.hammermetals.InfoHM;
import com.zeitheron.hammermetals.api.MetalData;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.jei.alloy.AlloyRecipe;
import com.zeitheron.hammermetals.jei.alloy.AlloyRecipeCategory;
import com.zeitheron.hammermetals.jei.alloy.AlloyRecipeWrapper;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.JEIPlugin;
import mezz.jei.api.recipe.IRecipeCategoryRegistration;

@JEIPlugin
public class JeiHM implements IModPlugin
{
	public static final String ALLOY_REGISTRY = InfoHM.MOD_ID + ":alloys";
	
	@Override
	public void register(IModRegistry registry)
	{
		registry.handleRecipes(AlloyRecipe.class, recipe -> new AlloyRecipeWrapper(recipe), ALLOY_REGISTRY);
		
		registry.addRecipes(MetalRegistry.metalSet().stream().map(MetalRegistry::getMetal).filter(MetalData::hasAlloy).map(AlloyRecipe::create).filter(Objects::nonNull).collect(Collectors.toList()), ALLOY_REGISTRY);
	}
	
	@Override
	public void registerCategories(IRecipeCategoryRegistration registry)
	{
		registry.addRecipeCategories(new AlloyRecipeCategory(registry.getJeiHelpers()));
	}
}