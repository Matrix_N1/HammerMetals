package com.zeitheron.hammermetals.collect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collector.Characteristics;

import javax.annotation.Nonnull;

import net.minecraft.util.NonNullList;

public class HMCollectors
{
	static final Set<Collector.Characteristics> CH_CONCURRENT_ID = Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.CONCURRENT, Collector.Characteristics.UNORDERED, Collector.Characteristics.IDENTITY_FINISH));
	static final Set<Collector.Characteristics> CH_CONCURRENT_NOID = Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.CONCURRENT, Collector.Characteristics.UNORDERED));
	static final Set<Collector.Characteristics> CH_ID = Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.IDENTITY_FINISH));
	static final Set<Collector.Characteristics> CH_UNORDERED_ID = Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.UNORDERED, Collector.Characteristics.IDENTITY_FINISH));
	
	/**
	 * Returns a {@code Collector} that accumulates the input elements into a
	 * new {@code NonNullList}. There are no guarantees on the type, mutability,
	 * serializability, or thread-safety of the {@code List} returned; if more
	 * control over the returned {@code List} is required, use
	 * {@link #toCollection(Supplier)}.
	 *
	 * @param <T>
	 *            the type of the input elements
	 * @return a {@code Collector} which collects all the input elements into a
	 *         {@code List}, in encounter order
	 */
	public static <T> Collector<T, ?, NonNullList<T>> toNNList()
	{
		return new CollectorImpl<>((Supplier<NonNullList<T>>) NonNullList::create, List::add, (left, right) ->
		{
			left.addAll(right);
			return left;
		}, CH_ID);
	}
	
	public static <I, R> Function<I, R> castingIdentity()
	{
		return i -> (R) i;
	}
	
	static class CollectorImpl<T, A, R> implements Collector<T, A, R>
	{
		private final Supplier<A> supplier;
		private final BiConsumer<A, T> accumulator;
		private final BinaryOperator<A> combiner;
		private final Function<A, R> finisher;
		private final Set<Characteristics> characteristics;
		
		CollectorImpl(Supplier<A> supplier, BiConsumer<A, T> accumulator, BinaryOperator<A> combiner, Function<A, R> finisher, Set<Characteristics> characteristics)
		{
			this.supplier = supplier;
			this.accumulator = accumulator;
			this.combiner = combiner;
			this.finisher = finisher;
			this.characteristics = characteristics;
		}
		
		CollectorImpl(Supplier<A> supplier, BiConsumer<A, T> accumulator, BinaryOperator<A> combiner, Set<Characteristics> characteristics)
		{
			this(supplier, accumulator, combiner, castingIdentity(), characteristics);
		}
		
		@Override
		public BiConsumer<A, T> accumulator()
		{
			return accumulator;
		}
		
		@Override
		public Supplier<A> supplier()
		{
			return supplier;
		}
		
		@Override
		public BinaryOperator<A> combiner()
		{
			return combiner;
		}
		
		@Override
		public Function<A, R> finisher()
		{
			return finisher;
		}
		
		@Override
		public Set<Characteristics> characteristics()
		{
			return characteristics;
		}
	}
}