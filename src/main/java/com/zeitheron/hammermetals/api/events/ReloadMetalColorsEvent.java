package com.zeitheron.hammermetals.api.events;

import it.unimi.dsi.fastutil.objects.Object2IntArrayMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import net.minecraftforge.fml.common.eventhandler.Event;

public class ReloadMetalColorsEvent extends Event
{
	public final Object2IntMap<String> colors = new Object2IntArrayMap<>();
}