package com.zeitheron.hammermetals.api.parts;

import java.util.Arrays;

public enum EnumBlockMetalPart
{
	BLOCK("block", 9F), //
	ORE("ore", 2F), //
	CASING("casing", 20F);
	
	public final String sub;
	public final String od;
	
	public float ingotsWorth;
	
	private EnumBlockMetalPart(String od, float ingotsWorth)
	{
		this.sub = name().toLowerCase();
		this.ingotsWorth = ingotsWorth;
		this.od = od;
	}
	
	public float getIngotsWorth()
	{
		return ingotsWorth;
	}
	
	public boolean notOre()
	{
		return this != ORE;
	}
	
	public static EnumBlockMetalPart[] sortByCost(boolean reverse)
	{
		EnumBlockMetalPart[] parts = values().clone();
		Arrays.sort(parts, (a, b) -> a.ingotsWorth > b.ingotsWorth ? 1 : a.ingotsWorth < b.ingotsWorth ? -1 : 0);
		return parts;
	}
}