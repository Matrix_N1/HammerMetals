package com.zeitheron.hammermetals.api;

import com.zeitheron.hammermetals.api.parts.EnumBlockMetalPart;
import com.zeitheron.hammermetals.api.parts.EnumItemMetalPart;

public class MetalBuilder
{
	private MetalInfo info;
	
	MetalBuilder(String id, String mod, boolean isAlloy)
	{
		info = new MetalInfo(id, mod, isAlloy ? new AlloyInfo() : null);
	}
	
	public MetalBuilder add(EnumItemMetalPart part)
	{
		info.itemParts.add(part);
		return this;
	}
	
	public MetalBuilder add(EnumBlockMetalPart part)
	{
		info.blockParts.add(part);
		return this;
	}
	
	@Deprecated
	public MetalBuilder addNugget()
	{
		add(EnumItemMetalPart.NUGGET);
		return this;
	}
	
	@Deprecated
	public MetalBuilder addRod()
	{
		add(EnumItemMetalPart.ROD);
		return this;
	}
	
	@Deprecated
	public MetalBuilder addPlate()
	{
		add(EnumItemMetalPart.PLATE);
		return this;
	}
	
	@Deprecated
	public MetalBuilder addGear()
	{
		add(EnumItemMetalPart.GEAR);
		return this;
	}
	
	@Deprecated
	public MetalBuilder addDust()
	{
		add(EnumItemMetalPart.DUST);
		return this;
	}
	
	@Deprecated
	public MetalBuilder addIngot()
	{
		add(EnumItemMetalPart.INGOT);
		return this;
	}
	
	@Deprecated
	public MetalBuilder addBlock()
	{
		add(EnumBlockMetalPart.BLOCK);
		return this;
	}
	
	public MetalBuilder withColor(int color)
	{
		info.useTextureTemplate = true;
		info.color = color;
		return this;
	}
	
	public MetalBuilder addOre(int maxInChunk, int minY, int maxY, int maxVeinSize)
	{
		add(EnumBlockMetalPart.ORE);
		info.maxInChunk = maxInChunk;
		info.minY = minY;
		info.maxY = maxY;
		info.maxVeinSize = maxVeinSize;
		return this;
	}
	
	public MetalInfo build()
	{
		MetalInfo i = info;
		info = null;
		return i;
	}
}