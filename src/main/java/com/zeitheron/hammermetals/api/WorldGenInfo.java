package com.zeitheron.hammermetals.api;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammercore.lib.zlib.json.serapi.IgnoreSerialization;
import com.zeitheron.hammercore.lib.zlib.json.serapi.Jsonable;
import com.zeitheron.hammercore.lib.zlib.json.serapi.SerializedName;

public class WorldGenInfo implements Jsonable
{
	@SerializedName("gen_source")
	public GenSource gen_src = new GenSource();
	public boolean biomeWhitelist = false;
	public boolean dimensionWhitelist = false;
	
	@IgnoreSerialization
	public List<String> biomes = new ArrayList<>();
	
	@IgnoreSerialization
	public List<Integer> dimensions = new ArrayList<>();
	
	@Override
	public String serialize()
	{
		String s = Jsonable.super.serialize();
		s = s.substring(0, s.length() - 1) + ",\"biomes\":[";
		for(String b : biomes)
			s += "\"" + Jsonable.formatInsideString(b) + "\",";
		if(s.endsWith(","))
			s = s.substring(0, s.length() - 1);
		s += "],\"dimensions\":[";
		for(Integer i : dimensions)
			s += i + ",";
		if(s.endsWith(","))
			s = s.substring(0, s.length() - 1);
		return s + "]}";
	}
}