package com.zeitheron.hammermetals.api;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import com.zeitheron.hammercore.lib.zlib.json.JSONArray;
import com.zeitheron.hammercore.lib.zlib.json.serapi.IgnoreSerialization;
import com.zeitheron.hammercore.lib.zlib.json.serapi.Jsonable;
import com.zeitheron.hammercore.lib.zlib.json.serapi.SerializationContext;
import com.zeitheron.hammermetals.api.parts.EnumBlockMetalPart;
import com.zeitheron.hammermetals.api.parts.EnumItemMetalPart;

/**
 * A result of building a new metal
 */
public class MetalInfo implements Jsonable
{
	public final String id;
	public final String mod;
	
	public final AlloyInfo alloy;
	
	public boolean useTextureTemplate;
	public boolean useOreTemplate;
	public int color;
	
	@IgnoreSerialization
	public boolean useGeneratedLang;
	
	@IgnoreSerialization
	public final LocaleTranslation langs = new LocaleTranslation();
	
	@IgnoreSerialization
	public final Set<EnumItemMetalPart> itemParts = new HashSet<>();
	
	@IgnoreSerialization
	public final Set<EnumBlockMetalPart> blockParts = new HashSet<>();
	
	public int maxInChunk, minY, maxY, maxVeinSize;
	
	public WorldGenInfo gen = new WorldGenInfo();
	
	@IgnoreSerialization
	public Consumer<MetalData> onFinish;
	
	MetalInfo(String id, String mod, AlloyInfo alloy)
	{
		this.id = id;
		this.mod = mod;
		this.alloy = alloy;
	}
	
	public String idUpper()
	{
		return Character.toUpperCase(id.charAt(0)) + id.substring(1);
	}
	
	public boolean add(EnumItemMetalPart part)
	{
		return itemParts.contains(part);
	}
	
	public boolean add(EnumBlockMetalPart part)
	{
		return blockParts.contains(part);
	}
	
	@Override
	public SerializationContext serializationContext()
	{
		SerializationContext ctx = new SerializationContext();
		
		JSONArray arr = new JSONArray();
		itemParts.stream().map(s -> s.name().toLowerCase()).forEach(arr::put);
		ctx.set("addItems", arr);
		
		arr = new JSONArray();
		blockParts.stream().map(s -> s.name().toLowerCase()).forEach(arr::put);
		ctx.set("addBlocks", arr);
		
		if(useGeneratedLang)
			ctx.set("langs", langs.toString());
		
		return ctx;
	}
}