package com.zeitheron.hammermetals.items;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class ItemHammer extends Item
{
	public static final ItemHammer HAMMER = new ItemHammer();
	
	public static ItemStack recipeItem()
	{
		return new ItemStack(HAMMER, 1, OreDictionary.WILDCARD_VALUE);
	}
	
	private ItemHammer()
	{
		this.setUnlocalizedName("hammer");
		this.setMaxStackSize(1);
		this.setMaxDamage(127);
	}
	
	@Override
	public boolean hasContainerItem(ItemStack stack)
	{
		return stack.getItem() == HAMMER;
	}
	
	@Override
	public ItemStack getContainerItem(ItemStack itemStack)
	{
		return new ItemStack(this, itemStack.getCount(), itemStack.getItemDamage() + 1);
	}
}